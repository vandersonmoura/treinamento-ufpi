package br.ufpi.carrinhocompras.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.DragDropEvent;

import br.ufpi.carrinhocompras.facade.CompraFacade;
import br.ufpi.carrinhocompras.model.Compra;
import br.ufpi.carrinhocompras.model.ItemCompra;
import br.ufpi.carrinhocompras.model.Produto;

@Named
@SessionScoped
public class CompraBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private List<ItemCompra> itensCompra;
	
	private Produto produtoSelecionado;
	
	@Inject
	private UsuarioBean usuarioBean;
	
	@Inject
	private CompraFacade compraFacade;
	
	@PostConstruct
	public void init(){
		itensCompra = new ArrayList<ItemCompra>();
	}
	
	public CompraBean() {
	}
	
	public void selecionarUmProduto(DragDropEvent ddEvent){
		Produto produto = (Produto) ddEvent.getData();
		inserirNoCarrinho(produto);
	}
	
	private void inserirNoCarrinho(Produto produto){
		boolean produtoExiste = false;
		for (ItemCompra itemCompra : itensCompra) {
			if(itemCompra.getProduto().getId().equals(produto.getId())){
				itemCompra.setQuantidade(itemCompra.getQuantidade()+1);
				produtoExiste = true;
				break;
			}
		}
		if(!produtoExiste){
			ItemCompra item = new ItemCompra();
			item.setProduto(produto);
			item.setQuantidade(1);
			itensCompra.add(item);
		}
	}
	
	public void adicionarNoCarrinho(Produto produto){
		inserirNoCarrinho(produto);
	}
	
	public void remover(ItemCompra itemCompra){
		itensCompra.remove(itemCompra);
	}
	
	public void finalizarCompra(){
		Compra compra = new Compra();
		compra.setData(new Date());
		compra.setProdutos(itensCompra);
		compra.setTipoPagamento("DINHEIRO");
		compra.setCliente(usuarioBean.getUsuarioLogado());
		double valor = 0;
		for (ItemCompra itemCompra : itensCompra) {
			valor += itemCompra.getQuantidade() * itemCompra.getProduto().getPrecoUnitario();
		}
		compra.setValor(valor);
		
		compraFacade.salvarCompra(compra);
		itensCompra = new ArrayList<ItemCompra>();
		produtoSelecionado = null;
	}

	public List<ItemCompra> getItensCompra() {
		return itensCompra;
	}

	public void setItensCompra(List<ItemCompra> itensCompra) {
		this.itensCompra = itensCompra;
	}

	public Produto getProdutoSelecionado() {
		return produtoSelecionado;
	}

	public void setProdutoSelecionado(Produto produtoSelecionado) {
		this.produtoSelecionado = produtoSelecionado;
	}
	
	
}
