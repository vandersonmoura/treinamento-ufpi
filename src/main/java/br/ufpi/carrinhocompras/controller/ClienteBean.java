package br.ufpi.carrinhocompras.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.ufpi.carrinhocompras.facade.ClienteFacade;
import br.ufpi.carrinhocompras.model.Cliente;

@Named
@RequestScoped
public class ClienteBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private ClienteFacade clienteFacade;

	private String cpf;

	private Cliente cliente;

	@PostConstruct
	private void init() {
		this.cliente = new Cliente();
	}

	public void salvar() {
		if (cpf != null) {
			cpf = cpf.replaceAll("\\.", "").replaceAll("\\-", "");
			cliente.setCpf(Long.valueOf(cpf));
			clienteFacade.salvar(this.cliente);
		}
		this.cliente = new Cliente();
		this.cpf = "";
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

}
