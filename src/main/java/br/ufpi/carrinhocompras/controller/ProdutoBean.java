package br.ufpi.carrinhocompras.controller;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import br.ufpi.carrinhocompras.facade.ProdutoFacade;
import br.ufpi.carrinhocompras.model.Produto;

@Named
@SessionScoped
public class ProdutoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Produto produto;
	
	private LazyDataModel<Produto> produtos;

	@Inject
	private ProdutoFacade produtoFacade;
	
	@PostConstruct
	public void init(){
		produto = new Produto();
		
		produtos = new LazyDataModel<Produto>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<Produto> load(int first, int pageSize,
					String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				this.setRowCount(produtoFacade.countProdutos());

				return produtoFacade.listarProdutosLazy(first, pageSize);
			}

		};
	}


	public ProdutoBean() {
		
	}

	public void salvar() {
		try {
			produtoFacade.salvar(produto);
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("form:desc",
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							e.getMessage(), "detalhe da mensagem"));
		}
		cancelar();
	}
	
	public void remover(Produto produto) {
		if (produto.getId() == this.produto.getId()) {
			cancelar();
		}
		produtoFacade.deletar(produto);
	}

	public void editar(Produto produto) {
		this.produto = produto;
	}
	
	public void cancelar() {
		produto = new Produto();
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public LazyDataModel<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(LazyDataModel<Produto> produtos) {
		this.produtos = produtos;
	}
	
}
