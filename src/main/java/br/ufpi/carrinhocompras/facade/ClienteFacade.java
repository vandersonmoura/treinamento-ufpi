package br.ufpi.carrinhocompras.facade;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.ufpi.carrinhocompras.dao.ClienteDao;
import br.ufpi.carrinhocompras.model.Cliente;

@Stateless
public class ClienteFacade implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	private ClienteDao clienteDao;
	
	public ClienteFacade() {
	}
	
	public Cliente procurarClientePorEmailSenha(String email, String senha) {
		return clienteDao.procurarClientePorEmailSenha(email, senha);
	}

	public void salvar(Cliente cliente) {
		String senha = clienteDao.convertStringToMd5(cliente.getSenha());
		cliente.setSenha(senha);
		
		clienteDao.salvar(cliente);
	}

}
