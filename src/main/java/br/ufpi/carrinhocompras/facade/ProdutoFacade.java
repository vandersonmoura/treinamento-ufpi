package br.ufpi.carrinhocompras.facade;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import ar.com.fdvs.dj.domain.entities.columns.AbstractColumn;
import br.ufpi.carrinhocompras.dao.ProdutoDao;
import br.ufpi.carrinhocompras.model.Produto;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Stateless
public class ProdutoFacade {

	@Inject
	private ProdutoDao produtoDao;

	public void salvar(Produto produto) {
		produtoDao.salvar(produto);
	}

	public void deletar(Produto produto) {
		produtoDao.deletar(produto);
	}

	public List<Produto> listarProdutosLazy(int primeiro, int tamPagina) {
		return produtoDao.listarProdutosLazy(primeiro, tamPagina);
	}

	public int countProdutos() {
		return produtoDao.countProdutos();
	}

	public void gerarRelatorio() {

		List<Produto> produtos = produtoDao.listarProdutos();

		JRDataSource source = new JRBeanCollectionDataSource(produtos);

		AbstractColumn colunaDescricao = ColumnBuilder.getNew()
				.setTitle("Descrição")
				.setColumnProperty("descricao", String.class).build();
		AbstractColumn colunaPreco = ColumnBuilder.getNew().setTitle("Preço")
				.setColumnProperty("precoUnitario", Double.class).build();

		FastReportBuilder frb = new FastReportBuilder();
		frb.setTitle("Relatório de Produtos");
		frb.addColumn(colunaDescricao);
		frb.addColumn(colunaPreco);

		frb.setPrintBackgroundOnOddRows(true);
		frb.setUseFullPageWidth(true);

		Map<String, Object> params = new HashMap<String, Object>();

		DynamicReport dr = frb.build();

		JasperPrint jasperPrint = null;

		try {
			JasperReport jr = DynamicJasperHelper.generateJasperReport(dr,
					new ClassicLayoutManager(), params);

			jasperPrint = JasperFillManager.fillReport(jr, params, source);

			String tmp = System.getProperty("java.io.tmpdir");

			JasperExportManager.exportReportToPdfFile(jasperPrint,
					tmp + "/Relatorio.pdf");
		} catch (JRException e) {
			e.printStackTrace();
		}
	}

	public byte[] gerarRelatorioIReport() {
		List<Produto> produtos = produtoDao.listarProdutos();
		JRDataSource datasource = new JRBeanCollectionDataSource(produtos);
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			return exportaRelatorio(datasource, "RelatorioDeProdutos.jrxml",
					"RelatorioDeProdutos.pdf", params);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private byte[] exportaRelatorio(JRDataSource datasource,
			String absolutePath, String nomeRelatorio,
			Map<String, Object> params) throws JRException {
		try {
			FileInputStream fileInputStream = (FileInputStream) Thread
					.currentThread().getContextClassLoader()
					.getResourceAsStream(absolutePath);
			JasperReport jasperReport = JasperCompileManager
					.compileReport(fileInputStream);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,
					params, datasource);
			
			return JasperExportManager.exportReportToPdf(jasperPrint);
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
