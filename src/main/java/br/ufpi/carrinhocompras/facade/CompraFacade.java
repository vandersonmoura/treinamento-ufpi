/**
 * 
 */
package br.ufpi.carrinhocompras.facade;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.ufpi.carrinhocompras.dao.CompraDao;
import br.ufpi.carrinhocompras.model.Compra;

/**
 * @author Ronyerison
 *
 */
@Stateless
public class CompraFacade implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Inject
	private CompraDao compraDao;
	
	public void salvarCompra(Compra compra){
		compraDao.salvarCompra(compra);
	}
}
