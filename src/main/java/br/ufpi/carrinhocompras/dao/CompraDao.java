package br.ufpi.carrinhocompras.dao;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.ufpi.carrinhocompras.model.Compra;

@Stateless
public class CompraDao implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@PersistenceContext
	private EntityManager em;
	
	public void salvarCompra(Compra compra) {
		em.merge(compra);
	}

}
