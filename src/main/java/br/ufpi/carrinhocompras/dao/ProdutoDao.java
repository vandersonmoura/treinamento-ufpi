package br.ufpi.carrinhocompras.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.ufpi.carrinhocompras.model.Produto;

@Stateless
public class ProdutoDao {
	
	@PersistenceContext
	private EntityManager em;
	
	public void deletar(Produto produto){
		em.remove(em.merge(produto));
	}
	
	public void salvar(Produto produto){
		em.merge(produto);
	}
	
	public List<Produto> listarProdutosLazy(int primeiro, int tamPagina){
		TypedQuery<Produto> query = em.createQuery(
				"SELECT p FROM Produto p", Produto.class);
		query.setFirstResult(primeiro);
		query.setMaxResults(tamPagina);
		
		return query.getResultList();
	}
	
	public List<Produto> listarProdutos(){
		TypedQuery<Produto> query = em.createQuery(
				"SELECT p FROM Produto p", Produto.class);
		
		return query.getResultList();
	}
	
	public int countProdutos(){
		TypedQuery<Long> queryCount = em.createQuery(
				"SELECT COUNT(p) FROM Produto p", Long.class);
		
		return queryCount.getSingleResult().intValue();
	}
}
